import math
import numpy as np
import h5py
import matplotlib.pyplot as plt
import scipy
from PIL import Image
from scipy import ndimage
import scipy.misc
import tensorflow as tf
from tensorflow.python.framework import ops
from mnist_data_utils import *
from propagation_forward import *
import sys
import time

def current_time():
    return time.strftime("[%Y-%m-%d %H:%M:%S]  ")

#===========================================================================
# MNIST

train_x_flatten, train_y, test_x_flatten, test_y, num_classes = load_mnist_data() # ORIGINAL
#test_x_flatten, test_y, train_x_flatten, train_y = load_mnist_data() # DELETE THIS LINE AFTER DONE

ind = 812

## View TRAIN Example
#digit = train_x_flatten[:,ind]
#digit = digit.reshape(28,28)
#plt.title('this is  --->   ' + str(train_y[:,ind]))
#plt.imshow(digit, cmap='gray')
#
#
## View TEST Example
#digit = test_x_flatten[:,ind]
#digit = digit.reshape(28,28)
#plt.title('this is  --->   ' + str(test_y[:,ind]))
#plt.imshow(digit, cmap='gray')


# Standardize data to have feature values between 0 and 1.
train_x = train_x_flatten/255.
test_x = test_x_flatten/255.

print ("train_x's shape: " + str(train_x.shape))
print ("test_x's shape: " + str(test_x.shape))


#########################
# Fit NEURAL NETWORK:

train_x = np.swapaxes(train_x,0,1)
train_x = train_x.reshape(59999,28,28, 1)
test_x = np.swapaxes(test_x,0,1)
test_x = test_x.reshape(9999,28,28, 1)

train_y = np.swapaxes(train_y,0,1)
test_y = np.swapaxes(test_y,0,1)


# View TRAIN Example
plt.title('this is  --->   ' + str(train_y[ind, :]))
plt.imshow(train_x[ind].squeeze(), cmap='gray') # .squeeze() for squiezing extra dimention for display

#===========================================================================



# Loading the data (signs)
#X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes = load_dataset()
#
#
## Example of a picture
#index = 3
#plt.imshow(X_train_orig[index])
#print ("y = " + str(np.squeeze(Y_train_orig[:, index])))
#
#
#X_train = X_train_orig/255.
#X_test = X_test_orig/255.
#Y_train = convert_to_one_hot(Y_train_orig, 6).T
#Y_test = convert_to_one_hot(Y_test_orig, 6).T
#print ("number of training examples = " + str(X_train.shape[0]))
#print ("number of test examples = " + str(X_test.shape[0]))
#print ("X_train shape: " + str(X_train.shape))
#print ("Y_train shape: " + str(Y_train.shape))
#print ("X_test shape: " + str(X_test.shape))
#print ("Y_test shape: " + str(Y_test.shape))
#conv_layers = {}


def model(X_train, Y_train, X_test, Y_test, learning_rate = 0.0001,
          num_epochs = 100, minibatch_size = 64, print_cost = True, restore = False):
    """
    Implements a three-layer ConvNet in Tensorflow:
    CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> FLATTEN -> FULLYCONNECTED
    
    Arguments:
    X_train -- training set, of shape (None, 64, 64, 3)
    Y_train -- test set, of shape (None, n_y = 6)
    X_test -- training set, of shape (None, 64, 64, 3)
    Y_test -- test set, of shape (None, n_y = 6)
    learning_rate -- learning rate of the optimization
    num_epochs -- number of epochs of the optimization loop
    minibatch_size -- size of a minibatch
    print_cost -- True to print the cost every 100 epochs
    
    Returns:
    train_accuracy -- real number, accuracy on the train set (X_train)
    test_accuracy -- real number, testing accuracy on the test set (X_test)
    parameters -- parameters learnt by the model. They can then be used to predict.
    """
    
    ops.reset_default_graph()                         # to be able to rerun the model without overwriting tf variables
    tf.set_random_seed(1)                             # to keep results consistent (tensorflow seed)
    seed = 3                                          # to keep results consistent (numpy seed)
    (m, n_H0, n_W0, n_C0) = X_train.shape             
    n_y = Y_train.shape[1]                            
    costs = []                                        # To keep track of the cost
    
    # Create Placeholders of the correct shape
    X, Y, lr, pkeep, step = create_placeholders(n_H0, n_W0, n_C0, n_y)
    
    # Initialize parameters
    parameters = initialize_parameters()
                
    # Forward propagation: Build the forward propagation in the tensorflow graph
    Y_hat, Y_hat_logits = forward_propagation(X, parameters, pkeep)
    
    # Cost function: Add cost function to tensorflow graph
    cost = compute_cost(Y_hat_logits, Y)
    
    # Backpropagation: Define the tensorflow optimizer. Use an AdamOptimizer that minimizes the cost.
#    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
    
    # Backpropagation: Define the tensorflow optimizer. Use an AdamOptimizer that minimizes the cost.
    # training step, the learning rate is a placeholder
    # the learning rate is: # 0.0001 + 0.003 * (1/e)^(step/2000)), i.e. exponential decay from 0.003->0.0001
    lr = 0.0001 +  tf.train.exponential_decay(0.003, step, 10, 1/math.e)
    optimizer = tf.train.AdamOptimizer(lr).minimize(cost)
    
    
    # Initialize all the variables globally
    init = tf.global_variables_initializer()
    
    # Add ops to save and restore all the variables.
    saver = tf.train.Saver()
    do_restore = False
    if (restore):
        do_restore = True # 
    
#    f_training_log = open('training_log.txt', 'w') # overwrite file
    f_training_log = open('training_log.txt', 'a') # append to file
    f_training_log.write("\n" + current_time() + "---> Starting new training\n")
    f_training_log.flush()

     
    # Start the session to compute the tensorflow graph
    with tf.Session() as sess:
           
        # Run the initialization
        sess.run(init)

        if (do_restore):
            do_restore = False
            # Restore variables from disk.
            saver.restore(sess, "./saved_model/model.ckpt")
            stats_restore = "Model restored."
            print(stats_restore)
            
            # Log stats restore stats to file
            f_training_log.write(current_time() + stats_restore + '\n')
            f_training_log.flush()

        
        
        # Do the training loop
        for epoch in range(num_epochs):

            minibatch_cost = 0.
            num_minibatches = int(m / minibatch_size) # number of minibatches of size minibatch_size in the train set
            seed = seed + 1
            minibatches = random_mini_batches(X_train, Y_train, minibatch_size, seed)

            for minibatch in minibatches:

                # Select a minibatch
                (minibatch_X, minibatch_Y) = minibatch
                # IMPORTANT: The line that runs the graph on a minibatch.
                # Run the session to execute the optimizer and the cost, the feedict should contain a minibatch for (X,Y).
                _ , temp_cost = sess.run([optimizer, cost], feed_dict={X: minibatch_X, Y: minibatch_Y, step: epoch, pkeep: 0.75})
                
                minibatch_cost += temp_cost / num_minibatches
                

            # Print the cost every epoch
            if print_cost == True and epoch % 1 == 0:
                # Get training statistics.
                stats = "Cost after epoch %i: %f" % (epoch, minibatch_cost)
                print(stats)
                # Print training statistics (on same line).
#                print('\r' + stats, end="")
#                sys.stdout.flush()
                
                # Log training statistics to file.
                f_training_log.write(current_time() + stats + '\n')
                f_training_log.flush()
                
                
            if print_cost == True and epoch % 1 == 0:
                costs.append(minibatch_cost)
            if epoch % 5 == 0 and epoch > 0:  
                # Save the variables to disk.
                save_path = saver.save(sess, "./saved_model/model.ckpt")
                stats_save = "Model saved in path: %s" % save_path
                print(stats_save)
                
                # Log save stats to file
                f_training_log.write(current_time() + stats_save + '\n')
                f_training_log.flush()

        
        
        # plot the cost
        plt.plot(np.squeeze(costs))
        plt.ylabel('cost')
        plt.xlabel('iterations (per tens)')
        plt.title("Learning rate =" + str(learning_rate))
        plt.show()

        # Calculate the correct predictions
        predict_op = tf.argmax(Y_hat, 1)
        correct_prediction = tf.equal(predict_op, tf.argmax(Y, 1))
        
        # Calculate accuracy on the test set
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        print(accuracy)
        train_accuracy = accuracy.eval({X: X_train, Y: Y_train, pkeep: 1.0})
        test_accuracy = accuracy.eval({X: X_test, Y: Y_test, pkeep: 1.0})
        
        stats_train_accuracy = "Train Accuracy: " + str(train_accuracy)
        stats_test_accuracy = "Test Accuracy:" + str(test_accuracy)
        print(stats_train_accuracy)
        print(stats_test_accuracy)
        
        # Log stats restore stats to file
        f_training_log.write(current_time() + stats_train_accuracy + '\n')
        f_training_log.write(current_time() + stats_test_accuracy + '\n')
        f_training_log.flush()

        return train_accuracy, test_accuracy, parameters
    
    
    
#_, _, parameters = model(train_x, train_y, test_x, test_y, num_epochs = 7)
#
#_, _, parameters = model(train_x, train_y, test_x, test_y, num_epochs = 2, restore = True)

        
#saveParametersFromSessionToDictionary()

## SAVE
#np.save('parameters.npy', parameters) 


## LOAD  
#parameters_from_file = np.load('parameters.npy').item()


####################################################
####################################################
####################################################
######## WORKED -------------------
# prediction from restored session
    
    
    
def predict_from_restored_session(X, Y):
    
    ops.reset_default_graph()                         # to be able to rerun the model without overwriting tf variables
    tf.set_random_seed(1)                             # to keep results consistent (tensorflow seed)
    seed = 3                                          # to keep results consistent (numpy seed)
    
#    x, y = create_placeholders(28, 28, 1, 10)
#    parameters = initialize_parameters()
#    Z3 = forward_propagation(x, parameters)
    x, y, lr, pkeep, step = create_placeholders(28, 28, 1, 10)
    parameters = initialize_parameters()
    AL, _ = forward_propagation(X, parameters, pkeep)

    
    init = tf.global_variables_initializer()
    
    saver = tf.train.Saver()        
     
    # Start the session to compute the tensorflow graph
    with tf.Session() as sess:
           
        # Run the initialization
        sess.run(init)

        # Restore variables from disk.
        saver.restore(sess, "./saved_model/model.ckpt")
        print("Model restored.")
        
        Y_hat = sess.run(AL, feed_dict = {x: X, pkeep: 1.0})

        predict_op = tf.argmax(Y_hat, 1)
        correct_prediction = tf.equal(predict_op, tf.argmax(Y, 1))
        
        # Calculate accuracy on the test set
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        print(accuracy)
        train_accuracy = accuracy.eval({x: train_x, y: train_y, pkeep: 1.0})
        test_accuracy = accuracy.eval({x: test_x, y: test_y, pkeep: 1.0})
        print("Train Accuracy:", train_accuracy)
        print("Test Accuracy:", test_accuracy)
                
        return Y_hat
    
###############
### Predict from test set
##############
indx = 251

image = test_x[indx].reshape(1,28,28,1)
label = test_y[indx].reshape(1,10)

pred_from_sess = predict_from_restored_session(image, label)

print("\n")
actual_number = np.argmax(test_y[indx, :])
print('Actual Number is  --->   ' + str(actual_number))
print("\n")

print("Prediction: " + str(pred_from_sess))

xmax, xmin = pred_from_sess.max(), pred_from_sess.min()
pred_normalized = (pred_from_sess - xmin)/(xmax - xmin)
print("After normalization:")
print(pred_normalized)
print("\n")
print("Predicted number   --->   " + str(np.argmax(pred_normalized)))


plt.title('this is  --->   ' + str(test_y[indx, :]))
plt.imshow(test_x[indx].squeeze(), cmap='gray') # .squeeze() for squiezing extra dimention for display

###############
### Predict from file
##############

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])

def get_image(path):
    ## rescale
    image = np.array(ndimage.imread(path, flatten=False))
    i_width = 28
    i_height = 28
    my_image = scipy.misc.imresize(image, (i_height, i_width))    
    #### convert into gray image
    gray_image = rgb2gray(my_image)    
    gray_image = gray_image[..., np.newaxis]  
    return gray_image

test_image = get_image("./mnist_test_images/3.png")
plt.imshow(test_image.squeeze(), cmap='gray')

image = test_image.reshape(1,28,28,1)
dummy_label = np.array([[0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]])


pred_from_sess = predict_from_restored_session(image, dummy_label)
print("Prediction: " + str(pred_from_sess))
xmax, xmin = pred_from_sess.max(), pred_from_sess.min()
pred_normalized = (pred_from_sess - xmin)/(xmax - xmin)
print("After normalization:")
print(pred_normalized)
print("\n")
print("Predicted number   --->   " + str(np.argmax(pred_normalized)))


















##################################
# Code below doesnt work :(
####################################################
##########   PREDICT FROM SAVED DICTIONARY   #######
####################################################
####################################################
def saveParametersFromSessionToDictionary():    
    parameters = initialize_parameters()
    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    with tf.Session() as sess:
        sess.run(init)
        # Restore variables from disk.
        saver.restore(sess, "./saved_model/model.ckpt")
        print("Model restored.")
        
        print(parameters["W1"].eval())
        parameters_dict = {"W1": parameters["W1"].eval(),
                           "W2": parameters["W2"].eval()}
        
        np.save('parameters.npy', parameters_dict)
        print("Dictionary Saved.")

####################################################
####################################################


parameters_dict = np.load('parameters.npy').item()

def predict_3(X, Y):
    
    ops.reset_default_graph()                         # to be able to rerun the model without overwriting tf variables
    tf.set_random_seed(1)                             # to keep results consistent (tensorflow seed)
    seed = 3                                          # to keep results consistent (numpy seed)
    
    x, y = create_placeholders(28, 28, 1, 10)
    
#    w1_const = tf.constant(parameters_dict["W1"])
#    W1 = tf.get_variable('W1', initializer=w1_const)
    W1 = tf.convert_to_tensor(parameters_dict["W1"])

#    w2_const = tf.constant(parameters_dict["W2"])
#    W2 = tf.get_variable('W2', initializer=w2_const)
    W2 = tf.convert_to_tensor(parameters_dict["W2"])

    parameters = {"W1": W1,
                  "W2": W2}

#    parameters = initialize_parameters()
                
    Z3 = forward_propagation(x, parameters)
    
    init = tf.global_variables_initializer()
    
#    saver = tf.train.Saver()        
     
    # Start the session to compute the tensorflow graph
    with tf.Session() as sess:
           
        # Run the initialization
        sess.run(init)

        # Restore variables from disk.
#        saver.restore(sess, "./saved_model/model.ckpt")
#        print("Model restored.")
        
        Y_hat = sess.run(Z3, feed_dict = {x: X})

        predict_op = tf.argmax(Y_hat, 1)
        correct_prediction = tf.equal(predict_op, tf.argmax(Y, 1))
        
        # Calculate accuracy on the test set
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        print(accuracy)
        train_accuracy = accuracy.eval({x: train_x, y: train_y})
        test_accuracy = accuracy.eval({x: test_x, y: test_y})
        print("Train Accuracy:", train_accuracy)
        print("Test Accuracy:", test_accuracy)
                
        return Y_hat


indx = 47

image = test_x[indx].reshape(1,28,28,1)
label = test_y[indx].reshape(1,10)

ppp = predict_3(image, label)

print("\n")
actual_number = np.argmax(test_y[indx, :])
print('Actual Number is  --->   ' + str(actual_number))
print("\n")

print("Prediction: " + str(ppp))

xmax, xmin = ppp.max(), ppp.min()
pred_normalized = (ppp - xmin)/(xmax - xmin)
print("After normalization:")
print(pred_normalized)
print("\n")
print("Predicted number   --->   " + str(np.argmax(pred_normalized)))


plt.title('this is  --->   ' + str(test_y[indx, :]))
plt.imshow(test_x[indx].squeeze(), cmap='gray') # .squeeze() for squiezing extra dimention for display


