import numpy as np
import pandas as pd
import math

def load_mnist_data():
    
    C = 10
    
    df_orig_train = pd.read_csv('../datasets/mnist/mnist_train_final.csv')
    df_orig_test = pd.read_csv('../datasets/mnist/mnist_test_final.csv')

    # TRAIN Data
    df_digits_train = df_orig_train.drop('label',axis=1)
    digits_data_train = df_digits_train.as_matrix()

    # TEST Data
    df_digits_test = df_orig_test.drop('label',axis=1)
    digits_data_test = df_digits_test.as_matrix()

    # TRAIN Labels
    labels_train_raw = df_orig_train['label'].as_matrix()
    labels_train = to_one_hot(labels_train_raw, C)

    # TEST Labels
    labels_test_raw = df_orig_test['label'].as_matrix()
    labels_test = to_one_hot(labels_test_raw, C)
    
    
    # TRANSPOSE Digits to match our model
    digits_data_train_tr = np.transpose(digits_data_train)
    digits_data_test_tr = np.transpose(digits_data_test)
    
    return digits_data_train_tr, labels_train, digits_data_test_tr, labels_test, C
    

def to_one_hot(Y, C):
    out_Y = np.zeros((C, Y.shape[0]))
    for i in range(len(Y)):
        label = Y[i]
        one_hot = np.zeros(C)
        one_hot[label] = 1
        out_Y[:,i] = one_hot
    return out_Y


###############################
    

def random_mini_batches(X, Y, mini_batch_size = 64, seed = 0):
    """
    Creates a list of random minibatches from (X, Y)
    
    Arguments:
    X -- input data, of shape (input size, number of examples) (m, Hi, Wi, Ci)
    Y -- true "label" vector (containing 0 if cat, 1 if non-cat), of shape (1, number of examples) (m, n_y)
    mini_batch_size - size of the mini-batches, integer
    seed -- this is only for the purpose of grading, so that you're "random minibatches are the same as ours.
    
    Returns:
    mini_batches -- list of synchronous (mini_batch_X, mini_batch_Y)
    """
    
    m = X.shape[0]                  # number of training examples
    mini_batches = []
    np.random.seed(seed)
    
    # Step 1: Shuffle (X, Y)
    permutation = list(np.random.permutation(m))
    shuffled_X = X[permutation,:,:,:]
    shuffled_Y = Y[permutation,:]

    # Step 2: Partition (shuffled_X, shuffled_Y). Minus the end case.
    num_complete_minibatches = math.floor(m/mini_batch_size) # number of mini batches of size mini_batch_size in your partitionning
    for k in range(0, num_complete_minibatches):
        mini_batch_X = shuffled_X[k * mini_batch_size : k * mini_batch_size + mini_batch_size,:,:,:]
        mini_batch_Y = shuffled_Y[k * mini_batch_size : k * mini_batch_size + mini_batch_size,:]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)
    
    # Handling the end case (last mini-batch < mini_batch_size)
    if m % mini_batch_size != 0:
        mini_batch_X = shuffled_X[num_complete_minibatches * mini_batch_size : m,:,:,:]
        mini_batch_Y = shuffled_Y[num_complete_minibatches * mini_batch_size : m,:]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)
    
    return mini_batches


def convert_to_one_hot(Y, C):
    Y = np.eye(C)[Y.reshape(-1)].T
    return Y

