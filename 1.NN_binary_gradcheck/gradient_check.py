import numpy as np
from propagation_forward import *
from propagation_backward import *
from data_utils import load_data
import pdb

def param_and_grad_to_vec(parameters, gradients):
    """
    Convert dictionary into a single concatenated vector.
    
    Flatten all parameters and concetenating them into one large vector.
    We store keys for that values in 'param_keys' and 'param_keys' arrays

    ["W1", "b1", "W2", "b2", "W3", "b3"] - example parameter keys
    ["dW1", "db1", "dW2", "db2", "dW3", "db3"] - example gradients keys
    """
    param_keys = []
    shapes = []
    count = 0
    
    for key, value in parameters.items():

        # flatten parameter
        param_new_vector = np.reshape(parameters[key], (-1,1))
        param_keys = param_keys + [key]*param_new_vector.shape[0] # sync index keys with param_new_vector
        
        # flatten gradient
        curren_grad_key = "d" + key # get gey by combining d+W1=dW1
        grad_new_vector = np.reshape(gradients[curren_grad_key], (-1,1))

        #Shapes
        assert (parameters[key].shape == gradients[curren_grad_key].shape) #shapes of weight should be same as gradients
        shapes.append(parameters[key].shape) 

        if count == 0:
            param_theta = param_new_vector
            grad_theta = grad_new_vector
        else:
            param_theta = np.concatenate((param_theta, param_new_vector), axis=0)
            grad_theta = np.concatenate((grad_theta, grad_new_vector), axis=0)
        count = count + 1

    return param_theta, param_keys, grad_theta, shapes


def vec_to_dict(param_theta, param_keys, shapes):
    """
    Unroll all our parameters dictionary from a single vector satisfying our specific required shape.
    """
    parameters = {}
    current_shape_index = 0
    param_start_index = 0
    
    for i in range(0, len(param_keys)):
        key = param_keys[i]
        param = param_theta[i]
        if i > 0 and i < len(param_keys)-1:

            next_key = param_keys[i + 1]
            if next_key != key:
                temp_param = param_theta[param_start_index:i+1 ,:]
                parameters[key] = temp_param.reshape(shapes[current_shape_index])
                    
                current_shape_index += 1
                param_start_index = i+1
                
        # if last key        
        elif i == len(param_keys)-1:
            temp_param = param_theta[param_start_index:i+1 ,:]
            parameters[key] = temp_param.reshape(shapes[current_shape_index])

    return parameters



##############################################################
### GRADIENT CHECK STUFF:
###

X_orig, Y, _, _, _ = load_data() # MNIST training data
parameters = np.load('parameters.npy').item()

# Reshape the training and test examples 
train_x_flatten = X_orig.reshape(X_orig.shape[0], -1).T   # The "-1" makes reshape flatten the remaining dimensions
# Standardize data to have feature values between 0 and 1.
X = train_x_flatten/255.


#print("--------------------------------------\n")
#print("parameters: " + str(parameters))
#
#param_theta, param_keys, grad_theta, shapes = param_and_grad_to_vec(parameters, gradients)
#
#par_convert_back = vec_to_dict(param_theta, param_keys, shapes)
#print("\n\n==============================================")
#print("par_convert_back: " + str(par_convert_back))




    
    
def forward_propagation_n(X, Y, parameters):
    AL, caches = L_model_forward(X, parameters)
    cost = compute_cost(AL, Y) 
    return cost, AL




def gradient_check_n(parameters, X, Y, epsilon = 1e-7):
    """
    Checks if backward_propagation_n computes correctly the gradient of the cost output by forward_propagation_n
    
    Arguments:
    parameters -- python dictionary containing your parameters "W1", "b1", "W2", "b2", "W3", "b3":
    grad -- output of backward_propagation_n, contains gradients of the cost with respect to the parameters. 
    x -- input datapoint, of shape (input size, 1)
    y -- true "label"
    epsilon -- tiny shift to the input to compute approximated gradient with formula(1)
    
    Returns:
    difference -- difference (2) between the approximated gradient and the backward propagation gradient
    """
    #Getting Gradients:
    AL, caches = L_model_forward(X, parameters)
    cost = compute_cost(AL, Y) 
    gradients = L_model_backward(AL, Y, caches)


    # Set-up variables
#     parameters_values, _ = dictionary_to_vector(parameters)
#     grad = gradients_to_vector(gradients)
    param_theta, param_keys, grad, shapes = param_and_grad_to_vec(parameters, gradients)

    num_parameters = param_theta.shape[0]
    J_plus = np.zeros((num_parameters, 1))
    J_minus = np.zeros((num_parameters, 1))
    gradapprox = np.zeros((num_parameters, 1))
    
    # Compute gradapprox
    for i in range(num_parameters):
        
        # Compute J_plus[i]. Inputs: "parameters_values, epsilon". Output = "J_plus[i]".
        # "_" is used because the function you have to outputs two parameters but we only care about the first one
        ### START CODE HERE ### (approx. 3 lines)
        thetaplus = np.copy(param_theta)                                            # Step 1
        thetaplus[i][0] = thetaplus[i][0] + epsilon 
#         J_plus[i], _ = forward_propagation_n(X, Y, vector_to_dictionary(thetaplus))       # Step 3
        J_plus[i], _ = forward_propagation_n(X, Y, vec_to_dict(thetaplus, param_keys, shapes))       # Step 3
        
        ### END CODE HERE ###
        
        # Compute J_minus[i]. Inputs: "parameters_values, epsilon". Output = "J_minus[i]".
        ### START CODE HERE ### (approx. 3 lines)
        thetaminus = np.copy(param_theta)                                           # Step 1
        thetaminus[i][0] = thetaminus[i][0] - epsilon                                     # Step 2        
#         J_minus[i], _ = forward_propagation_n(X, Y, vector_to_dictionary(thetaminus))     # Step 3
        J_minus[i], _ = forward_propagation_n(X, Y, vec_to_dict(thetaminus, param_keys, shapes))     # Step 3
        ### END CODE HERE ###
        
        # Compute gradapprox[i]
        ### START CODE HERE ### (approx. 1 line)
        gradapprox[i] = (J_plus[i] - J_minus[i]) / (2 * epsilon)
        ### END CODE HERE ###

        if i % 1 == 0:
            print (" %i th iteration: of total %i" %(i, num_parameters))
    
    # Compare gradapprox to backward propagation gradients by computing difference.
    ### START CODE HERE ### (approx. 1 line)
    numerator = np.linalg.norm(grad - gradapprox)                               # Step 1'
    denominator = np.linalg.norm(grad) + np.linalg.norm(gradapprox)             # Step 2'
    difference = numerator / denominator                                        # Step 3'
    ### END CODE HERE ###
    
#    pdb.set_trace()
    
    print("Numerator: " + str(numerator))
    print("denominator: " + str(denominator))
    print("difference: " + str(difference))
    
    if difference > 2e-7:
        print ("\033[93m" + "There is a mistake in the backward propagation! difference = " + str(difference) + "\033[0m")
    else:
        print ("\033[92m" + "Your backward propagation works perfectly fine! difference = " + str(difference) + "\033[0m")
    
    return difference


### CHECKING GRADIENTS:
difference = gradient_check_n(parameters, X, Y)