import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def load_data():
    
    C = 10
    
    df_orig_train = pd.read_csv('../datasets/mnist/mnist_train_final.csv')
    df_orig_test = pd.read_csv('../datasets/mnist/mnist_test_final.csv')

    # TRAIN Data
    df_digits_train = df_orig_train.drop('label',axis=1)
    digits_data_train = df_digits_train.as_matrix()

    # TEST Data
    df_digits_test = df_orig_test.drop('label',axis=1)
    digits_data_test = df_digits_test.as_matrix()

    # TRAIN Labels
    labels_train_raw = df_orig_train['label'].as_matrix()
    labels_train = convert_to_one_hot(labels_train_raw, C)

    # TEST Labels
    labels_test_raw = df_orig_test['label'].as_matrix()
    labels_test = convert_to_one_hot(labels_test_raw, C)
    
    
    # TRANSPOSE Digits to match our model
    digits_data_train_tr = np.transpose(digits_data_train)
    digits_data_test_tr = np.transpose(digits_data_test)
    
    return digits_data_train_tr, labels_train, digits_data_test_tr, labels_test
    

def convert_to_one_hot(Y, C):
    out_Y = np.zeros((C, Y.shape[0]))
    for i in range(len(Y)):
        label = Y[i]
        one_hot = np.zeros(C)
        one_hot[label] = 1
        out_Y[:,i] = one_hot
    return out_Y
