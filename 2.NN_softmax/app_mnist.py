import numpy as np
import matplotlib.pyplot as plt

from data_utils import load_data
from propagation_forward import *
from propagation_backward import *

train_x_flatten, train_y, test_x_flatten, test_y = load_data() # ORIGINAL

ind = 865

# # View TRAIN Example
# digit = train_x_flatten[:,ind]
# digit = digit.reshape(28,28)
# plt.title('this is  --->   ' + str(train_y[:,ind]))
# plt.imshow(digit, cmap='gray')


# # View TEST Example
# digit = test_x_flatten[:,ind]
# digit = digit.reshape(28,28)
# plt.title('this is  --->   ' + str(test_y[:,ind]))
# plt.imshow(digit, cmap='gray')

#===========================================================================


# Standardize data to have feature values between 0 and 1.
train_x = train_x_flatten/255.
test_x = test_x_flatten/255.

print ("train_x's shape: " + str(train_x.shape))
print ("test_x's shape: " + str(test_x.shape))


### CONSTANTS ###
C = 10 # 10 digits
layers_dims = [784, 2, 2, 2, 2, C] #  4-layer model




def L_layer_model(X, Y, layers_dims, learning_rate = 0.001, num_iterations = 3000, print_cost=False):#lr was 0.009
    """
    Implements a L-layer neural network: [LINEAR->RELU]*(L-1)->LINEAR->SIGMOID.
    
    Arguments:
    X -- data, numpy array of shape (number of examples, num_px * num_px)
    Y -- one hot vector containing digit
    layers_dims -- list containing the input size and each layer size, of length (number of layers + 1).
    learning_rate -- learning rate of the gradient descent update rule
    num_iterations -- number of iterations of the optimization loop
    print_cost -- if True, it prints the cost every 100 steps
    
    Returns:
    parameters -- parameters learnt by the model. They can then be used to predict.
    """

    np.random.seed(1)
    costs = []                         # keep track of cost
    
    # Parameters initialization. (≈ 1 line of code)
    parameters = initialize_parameters_deep(layers_dims)
    
    # Loop (gradient descent)
    for i in range(0, num_iterations):

        # Forward propagation: [LINEAR -> RELU]*(L-1) -> LINEAR -> SIGMOID.
        AL, caches, _ = L_model_forward(X, parameters)
        
        # Compute cost.
        cost = compute_softmax_cost(AL, Y)
    
        # Backward propagation.
        grads = L_model_backward(AL, Y, caches)
 
        # Update parameters.
        parameters = update_parameters(parameters, grads, learning_rate)
                
        # Print the cost every 100 training example
        if print_cost and i % 1 == 0:
            print ("Cost after iteration %i: %f" %(i, cost))
            costs.append(cost)
            
    # plot the cost
    # plt.plot(np.squeeze(costs))
    # plt.ylabel('cost')
    # plt.xlabel('iterations (per tens)')
    # plt.title("Learning rate =" + str(learning_rate))
    # plt.show()

    ## SAVE
    np.save('parameters.npy', parameters) 
    return parameters


## RUN THE MODEL
parameters = L_layer_model(train_x, train_y, layers_dims, num_iterations = 100, print_cost = True)


## LOAD  
#parameters = np.load('parameters.npy').item()


# print("\n")
# print ("-->On the train set:")
# predictions_train = predict(train_x, train_y, parameters)
# print ("-->On the test set:")
# predictions_test = predict(test_x, test_y, parameters)
# print("\n")



